package de.cmlab.ubicom.common;

import java.io.*;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.TooManyListenersException;

import gnu.io.*;

/**
 * SerialInterface - used for connection via com port to the electronic sensor
 * board
 * 
 * @author pete
 * 
 */
public class SerialInterface {
	private static CommPortIdentifier comPortId;
	private static SerialPort serialPort;
	private InputStream in;
	private OutputStream out;
	private boolean allowOutput = false;
	// using a Deque as stack for messages from the serial link
	private Deque<String> serialMessages;

	/**
	 * Constructor
	 * 
	 * @param comPort
	 *            the com port that should be used
	 * @throws NoSuchPortException
	 * @throws PortInUseException
	 */
	public SerialInterface(String comPort) throws NoSuchPortException,
			PortInUseException {
		comPortId = CommPortIdentifier.getPortIdentifier(comPort);
		serialPort = (SerialPort) comPortId.open("SerialConnection", 2000);
		this.allowOutput = false;
		this.serialMessages = new ArrayDeque<String>();
	}

	/**
	 * initiates and establishes the connection
	 */
	public void connect() {
		// get streams
		try {
			in = serialPort.getInputStream();
			out = serialPort.getOutputStream();
		} catch (IOException e) {
			throw new RuntimeException("Could not open Input/Output!");
		}

		// add event listener
		try {
			serialPort.addEventListener(new SerialPortEventListener() {

				@Override
				public void serialEvent(SerialPortEvent event) {
					if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
						// reads data trough a buffered reader and prints it on
						// the console
						BufferedReader stringInput = new BufferedReader(
								new InputStreamReader(in));
						String data = "";

						try {
							data = stringInput.readLine();
							if (allowOutput) {
								System.out.println(data);
							}
							serialMessages.push(data);
							stringInput.close();
						} catch (IOException e) {
							// something went wrong while receiving
							serialMessages.push("IOException while receiving");
						}

					}
				}
			});
		} catch (TooManyListenersException e) {
			throw new RuntimeException(e.getMessage());
		}

		serialPort.notifyOnDataAvailable(true);

		// establish connection
		try {
			serialPort.setSerialPortParams(115200, SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		} catch (UnsupportedCommOperationException e) {
			throw new RuntimeException("Could not set serial port parameters!");
		}
	}

	/**
	 * sends a command to the ESB
	 * 
	 * @param command
	 *            command string that is send over the serial connection
	 */
	public void sendCommand(String command) {
		PrintWriter comOut = new PrintWriter(out);
		comOut.println(command);
		comOut.flush();
		comOut.close();
	}

	/**
	 * enables console output
	 */
	public void allowOutput() {
		allowOutput = true;
	}

	/**
	 * disables console output
	 */
	public void disableOutput() {
		allowOutput = false;
	}

	/**
	 * closes the serial link
	 */
	public void closeConnection() {
		serialPort.close();
	}

	/**
	 * reads the last received message which was received from the serial link
	 * 
	 * @return string containing the last line read from the serial link
	 */
	public String getLastData() {
		String data = "";
		data = serialMessages.pop();
		return data;
	}
}
