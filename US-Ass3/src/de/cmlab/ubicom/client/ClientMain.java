package de.cmlab.ubicom.client;

import java.awt.EventQueue;

import javax.swing.UIManager;

/**
 * Client main class for start
 * 
 * @author Pete
 * 
 */
public class ClientMain {

	public static void main(String[] args) {
		// create gui and start it
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager
							.getSystemLookAndFeelClassName());
					ClientGui window = new ClientGui();
					window.show();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
