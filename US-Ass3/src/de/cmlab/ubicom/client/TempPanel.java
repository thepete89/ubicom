package de.cmlab.ubicom.client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * Custom extension of JPanel which generates a graph from float values
 * 
 * @author Pete
 * 
 */
public class TempPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private int zeroHeight;
	private float xOffset;
	private ArrayList<Point2D.Float> dataPoints;

	public TempPanel() {
		setBorder(BorderFactory.createLineBorder(Color.black));
		setBackground(Color.white);
		dataPoints = new ArrayList<>();
		xOffset = 20;
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(250, 200);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		// draw zero-line
		zeroHeight = getHeight() / 2;
		g2.setColor(Color.BLUE);
		g2.drawLine(0, zeroHeight, getWidth(), zeroHeight);

		if (dataPoints.size() > 1) {
			g2.setColor(Color.RED);
			GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD,
					dataPoints.size());

			path.moveTo(dataPoints.get(0).x, dataPoints.get(0).y);

			for (int i = 1; i < dataPoints.size(); i++) {
				path.lineTo(dataPoints.get(i).x, dataPoints.get(i).y);
			}

			g2.draw(path);
		}

	}

	/**
	 * adds a data point to the graph
	 * 
	 * @param value
	 *            the value of the point, used for the y coordinate
	 */
	public void addPoint(float value) {
		zeroHeight = getHeight() / 2;
		if (dataPoints.isEmpty()) {
			// create start point
			float xCoord = 0;
			float yCoord = zeroHeight - value;

			Point2D.Float point = new Point2D.Float(xCoord, yCoord);
			dataPoints.add(point);
		} else {
			// check if points get too much to display
			if (((dataPoints.size() + 1) * xOffset) > getWidth()) {
				dataPoints.clear();

				float xCoord = 0;
				float yCoord = zeroHeight - value;

				Point2D.Float point = new Point2D.Float(xCoord, yCoord);
				dataPoints.add(point);
			} else {
				// add data points
				Point2D.Float lastPoint = dataPoints.get(dataPoints.size() - 1);
				float xCoord = lastPoint.x + xOffset;
				float yCoord = zeroHeight - value;
				Point2D.Float point = new Point2D.Float(xCoord, yCoord);
				dataPoints.add(point);
			}
		}
		repaint();
	}
}
