package de.cmlab.ubicom.client;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import org.apache.xmlrpc.WebServer;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.Vector;

/**
 * Main GUI class for sensation client
 * 
 * @author Pete
 * 
 */
public class ClientGui {

	private JFrame frmSensationalGui;
	private JTextField rpcServerAddress;
	private JLabel lblPort;
	private JTextField rpcPort;
	private JButton btnConnect;
	private JLabel lblAvailableSensors;
	private JComboBox<String> sensorSelect;
	private JButton btnSelect;
	private TempPanel tempPanel;
	private JLabel lblLastValue;
	private JLabel lblInfoData;
	private XmlRpcClient rpcClient;
	private WebServer rpcServer;

	/**
	 * Create the application.
	 */
	public ClientGui() {
		initialize();
	}

	// initialize contents of the frame
	private void initialize() {
		frmSensationalGui = new JFrame();
		frmSensationalGui.setPreferredSize(new Dimension(300, 300));
		frmSensationalGui.setTitle("Sens-ational GUI");
		frmSensationalGui.setBounds(100, 100, 450, 330);
		frmSensationalGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 130, 6, 0, -25, 50, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 25, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				1.0, 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
				Double.MIN_VALUE };
		frmSensationalGui.getContentPane().setLayout(gridBagLayout);

		JLabel lblServer = new JLabel("RPC-Server:");
		GridBagConstraints gbc_lblServer = new GridBagConstraints();
		gbc_lblServer.anchor = GridBagConstraints.EAST;
		gbc_lblServer.fill = GridBagConstraints.VERTICAL;
		gbc_lblServer.insets = new Insets(0, 0, 5, 5);
		gbc_lblServer.gridx = 1;
		gbc_lblServer.gridy = 1;
		frmSensationalGui.getContentPane().add(lblServer, gbc_lblServer);

		rpcServerAddress = new JTextField();
		GridBagConstraints gbc_rpcServer = new GridBagConstraints();
		gbc_rpcServer.fill = GridBagConstraints.HORIZONTAL;
		gbc_rpcServer.insets = new Insets(0, 0, 5, 5);
		gbc_rpcServer.gridx = 2;
		gbc_rpcServer.gridy = 1;
		frmSensationalGui.getContentPane().add(rpcServerAddress, gbc_rpcServer);
		rpcServerAddress.setColumns(10);

		lblPort = new JLabel("Port:");
		GridBagConstraints gbc_lblPort = new GridBagConstraints();
		gbc_lblPort.gridwidth = 2;
		gbc_lblPort.insets = new Insets(0, 0, 5, 5);
		gbc_lblPort.gridx = 3;
		gbc_lblPort.gridy = 1;
		frmSensationalGui.getContentPane().add(lblPort, gbc_lblPort);

		rpcPort = new JTextField();
		GridBagConstraints gbc_rpcPort = new GridBagConstraints();
		gbc_rpcPort.fill = GridBagConstraints.HORIZONTAL;
		gbc_rpcPort.insets = new Insets(0, 0, 5, 5);
		gbc_rpcPort.gridx = 5;
		gbc_rpcPort.gridy = 1;
		frmSensationalGui.getContentPane().add(rpcPort, gbc_rpcPort);
		rpcPort.setColumns(10);

		btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rpcServerAddress.getText().isEmpty()
						|| rpcPort.getText().isEmpty()) {
					showErrorDialog(
							"No valid server address and/or no port given!",
							"Invalid connection data");
				} else {
					rpcConnect(rpcServerAddress.getText(), rpcPort.getText());
				}
			}
		});
		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
		gbc_btnConnect.insets = new Insets(0, 0, 5, 5);
		gbc_btnConnect.gridx = 6;
		gbc_btnConnect.gridy = 1;
		frmSensationalGui.getContentPane().add(btnConnect, gbc_btnConnect);

		btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (sensorSelect.getSelectedItem().equals("None")) {
					showErrorDialog(
							"No valid Sensor is selected! Do you have a connection to a sens-ation server?",
							"Invalid Sensor");
				} else {
					rpcRegister((String) sensorSelect.getSelectedItem());
				}
			}
		});

		lblAvailableSensors = new JLabel("Available Sensors:");
		GridBagConstraints gbc_lblAvailableSensors = new GridBagConstraints();
		gbc_lblAvailableSensors.anchor = GridBagConstraints.EAST;
		gbc_lblAvailableSensors.insets = new Insets(0, 0, 5, 5);
		gbc_lblAvailableSensors.gridx = 2;
		gbc_lblAvailableSensors.gridy = 2;
		frmSensationalGui.getContentPane().add(lblAvailableSensors,
				gbc_lblAvailableSensors);

		sensorSelect = new JComboBox<String>();
		sensorSelect.setModel(new DefaultComboBoxModel<String>(
				new String[] { "None" }));
		sensorSelect.setMaximumRowCount(1);
		GridBagConstraints gbc_sensorSelect = new GridBagConstraints();
		gbc_sensorSelect.gridwidth = 3;
		gbc_sensorSelect.insets = new Insets(0, 0, 5, 5);
		gbc_sensorSelect.fill = GridBagConstraints.HORIZONTAL;
		gbc_sensorSelect.gridx = 3;
		gbc_sensorSelect.gridy = 2;
		frmSensationalGui.getContentPane().add(sensorSelect, gbc_sensorSelect);
		GridBagConstraints gbc_btnSelect = new GridBagConstraints();
		gbc_btnSelect.insets = new Insets(0, 0, 5, 5);
		gbc_btnSelect.gridx = 6;
		gbc_btnSelect.gridy = 2;
		frmSensationalGui.getContentPane().add(btnSelect, gbc_btnSelect);

		tempPanel = new TempPanel();
		GridBagConstraints gbc_tempPanel = new GridBagConstraints();
		gbc_tempPanel.insets = new Insets(0, 0, 5, 5);
		gbc_tempPanel.gridwidth = 6;
		gbc_tempPanel.fill = GridBagConstraints.BOTH;
		gbc_tempPanel.gridx = 1;
		gbc_tempPanel.gridy = 3;
		frmSensationalGui.getContentPane().add(tempPanel, gbc_tempPanel);

		lblLastValue = new JLabel("Last Info:");
		GridBagConstraints gbc_lblLastValue = new GridBagConstraints();
		gbc_lblLastValue.insets = new Insets(0, 0, 5, 5);
		gbc_lblLastValue.gridx = 1;
		gbc_lblLastValue.gridy = 4;
		frmSensationalGui.getContentPane().add(lblLastValue, gbc_lblLastValue);

		lblInfoData = new JLabel("0.0");
		GridBagConstraints gbc_lblInfoData = new GridBagConstraints();
		gbc_lblInfoData.gridwidth = 5;
		gbc_lblInfoData.insets = new Insets(0, 0, 5, 5);
		gbc_lblInfoData.gridx = 2;
		gbc_lblInfoData.gridy = 4;
		frmSensationalGui.getContentPane().add(lblInfoData, gbc_lblInfoData);
	}

	/**
	 * show the gui
	 */
	public void show() {
		frmSensationalGui.setVisible(true);
	}

	/**
	 * update information label
	 * 
	 * @param data
	 *            data to display
	 */
	public void updateInfoLabel(String data) {
		lblInfoData.setText(data);
	}

	/**
	 * updates the displayed graph after parsing the given data
	 * 
	 * @param data
	 *            data to be graphically represented and parsed
	 */
	public void updateGraph(String data) {
		tempPanel.addPoint(Float.parseFloat(data));
	}

	/*
	 * private helper methods - showErrorDialog displays a
	 * JOptionPane-Error-Dialog - rpcConnect handles connection to the
	 * sensation-server - rpcRegister creates the local rpc server which
	 * sensation uses to inform our program about changes
	 */
	private void showErrorDialog(String message, String title) {
		JOptionPane.showMessageDialog(frmSensationalGui, message, title,
				JOptionPane.ERROR_MESSAGE);
	}

	private void rpcConnect(String server, String port) {
		try {
			rpcClient = new XmlRpcClient(server, Integer.parseInt(port));
			// receiving available sensors
			Object sensorData = rpcClient.execute(
					"GatewayXMLRPC.getAllSensorsVector", new Vector<>());
			if (sensorData instanceof Vector<?>) {
				@SuppressWarnings("unchecked")
				Vector<String> sensors = (Vector<String>) sensorData;

				sensorSelect.removeAllItems();
				for (String sensor : sensors) {
					sensorSelect.addItem(sensor);
				}
			} else {
				showErrorDialog(
						"Connection to server failed! Received invalid data as response!",
						"Connect failed");
			}

		} catch (NumberFormatException | MalformedURLException e) {
			showErrorDialog(
					"Connection to server failed! Please check your connection data.",
					"Connect failed");
		} catch (XmlRpcException e) {
			showErrorDialog(
					"Connection to server failed! The server threw an exception!",
					"Connect failed");
		} catch (IOException e) {
			showErrorDialog(
					"Connection to server failed! Please check your network or if the server is running.",
					"Connect failed");
		}
	}

	private void rpcRegister(String sensor) {
		try {
			// load port config
			Properties config = new Properties();
			InputStream is = new FileInputStream("ClientConfig.txt");
			config.load(is);
			String port = config.getProperty("rpcport", "8080");

			// create rpc server and handler
			StableXMLRPCClient handler = new StableXMLRPCClient(this);
			rpcServer = new WebServer(new Integer(port));
			rpcServer.addHandler("StableXMLRPCClient", handler);
			rpcServer.start();

			// get local network address - DANGER: could potentially fail and
			// return a false address if executing machine has more than one
			// connected network interface
			InetAddress local = InetAddress.getLocalHost();

			// prepare parameter vector
			Vector<String> params = new Vector<>();
			params.add(local.getHostAddress());
			params.add(sensor);
			params.add(port);

			// register
			String response = (String) rpcClient.execute(
					"GatewayXMLRPC.register", params);
			if ("error".equals(response)) {
				showErrorDialog("Registration at server for sensor " + sensor
						+ " failed!", "Registration failed");
			} else {
				lblInfoData.setText("Registration for Sensor " + sensor
						+ " complete");
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlRpcException e) {
			showErrorDialog("Registration at server for sensor " + sensor
					+ " failed!", "Registration failed");
		}
	}

}
