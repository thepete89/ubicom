package de.cmlab.ubicom.client;

/**
 * XML-RPC handler class which processes the information from the sensation
 * server
 * 
 * @author Pete
 * 
 */
public class StableXMLRPCClient {

	private ClientGui userInterface;

	/**
	 * Constructor
	 * 
	 * @param userInterface
	 *            instance of the user interface window, used for callback
	 */
	public StableXMLRPCClient(ClientGui userInterface) {
		this.userInterface = userInterface;
	}

	/**
	 * processes incoming data and passes it over to the gui
	 * 
	 * @param sensorID
	 *            id of the sensor from which the data is
	 * @param dateStamp
	 *            date when the data was measured
	 * @param value
	 *            the value that was measured
	 * @return simple string, in this case just OK, for xml rpc
	 */
	public String notify(String sensorID, String dateStamp, String value) {
		userInterface.updateInfoLabel(sensorID + " - " + dateStamp + " - "
				+ value);
		userInterface.updateGraph(value);
		return "OK";
	}

}
