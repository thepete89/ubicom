package de.cmlab.ubicom.sensor;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.Vector;

import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

/**
 * Main class for retrieving data from esbs temperature sensor, which sends it
 * over to Sensation
 * 
 * @author pete
 * 
 */
public class SensorMain {

	private static XmlRpcClient rpcConnection;
	private static String sensorId;
	private static SensorController sensorController;

	public static void main(String[] args) {
		// load config
		System.out
				.println("#### Sensor-Interface starting - Press CTRL+C to stop ####");
		System.out
				.println(" * Reading configuration from file SensorConfig.txt");

		Properties config = new Properties();
		InputStream is;

		try {
			is = new FileInputStream("SensorConfig.txt");
			config.load(is);
		} catch (FileNotFoundException e) {
			System.err
					.println("  - [WARNING]: Configuration File not found - using defaults.");
		} catch (IOException e) {
			System.err
					.println("  - [ERROR]: IOError while loading config - stopping..");
			System.exit(-1);
		}

		// esb connection
		System.out.println(" * Establishing connection to sensor board");
		try {
			sensorController = new SensorController(config.getProperty("serialport", "/dev/ttyUSB0"));
			sensorController.openConnection();
		} catch (NoSuchPortException | PortInUseException e2) {
			System.err.println("  - [ERROR]: Could not create serial link to ESB - stopping..");
			System.exit(-1);
		}
		
		
		// sensation xml-rpc
		System.out
				.println(" * Connecting to sens-ation server at "
						+ config.getProperty("server.address",
								"http://localhost:5000"));
		try {
			rpcConnection = new XmlRpcClient(config.getProperty(
					"server.address", "http://localhost:5000"));
		} catch (MalformedURLException e) {
			System.err
					.println("  - [ERROR]: Server address was malformed - stopping..");
			System.exit(-1);
		}

		System.out.println(" * Checking for running SensorPort on server side");
		try {
			String response = (String) rpcConnection.execute("SensorPort.ping",
					new Vector<>());
			if (!response.isEmpty()) {
				System.out.println(" * SensorPort running");
			}
		} catch (XmlRpcException e) {
			System.err.println("  - [ERROR]: XmlRpcException while execution: "
					+ e.getMessage());
			System.exit(-1);
		} catch (IOException e) {
			System.err
					.println("  - [ERROR]: IOException while sending request - is your network up and the server running?");
			System.exit(-1);
		}

		// register sensor
		String sensorXml = "<Sensor id=\""
				+ config.getProperty("sensor.id", "DefaultSensor") + "\" class=\""
				+ config.getProperty("sensor.class", "Other") + "\" >";
		sensorXml += "<HardwareId />";
		sensorXml += "<Command />";
		sensorXml += "<LocationID>"
				+ config.getProperty("sensor.location", "Mobile")
				+ "</LocationID>";
		sensorXml += "<Owner>" + config.getProperty("sensor.owner", "nobody")
				+ "</Owner>";
		sensorXml += "<Comment>"
				+ config.getProperty("sensor.comment", "Default comment")
				+ "</Comment>";
		sensorXml += "<AvailableSince>"
				+ config.getProperty("sensor.availableSince",
						"0000-00-00 00:00:00") + "</AvailableSince>";
		sensorXml += "<AvailableUntil>"
				+ config.getProperty("sensor.availableUntil",
						"0000-00-00 00:00:00") + "</AvailableUntil>";
		sensorXml += "<SensorActivity activity=\"active\" />";
		sensorXml += "<NativeDataType>"
				+ config.getProperty("sensor.datatype", "String")
				+ "</NativeDataType>";
		sensorXml += "<MaximumValue>"
				+ config.getProperty("sensor.maximum", "0.0")
				+ "</MaximumValue>";
		sensorXml += "<MinimumValue>"
				+ config.getProperty("sensor.minimum", "0.0")
				+ "</MinimumValue>";
		sensorXml += "</Sensor>";
		
		Vector<String> registration = new Vector<String>();
		registration.addElement(sensorXml);
		
		try {
			String response = (String) rpcConnection.execute("SensorPort.registerSensor", registration);
			
			if(!response.isEmpty()){
				System.out.println(" * Sensor registered as " + response);
				sensorId = response;
			}
		} catch (XmlRpcException | IOException e1) {
			System.err.println("  - [ERROR]: Could not register sensor with sensation!");
			e1.printStackTrace();
			System.exit(-1);
		}
		
		// main check loop - running forever
		int timeout = new Integer(config.getProperty("sensor.timeout", "10"));
		while (true) {
			try {
				Vector<Object> params = new Vector<Object>();
				params.addElement(sensorId);
				params.addElement("");
				params.addElement(sensorController.readTemperature());
				rpcConnection.execute("SensorPort.notify", params);
				
				Thread.sleep(timeout * 1000);
			} catch (InterruptedException | XmlRpcException | IOException e) {
				e.printStackTrace();
			}
		}

	}
}
