/**
 * This package contains the sensor manager which communicates with the electronic
 * sensor board and sends the measured data to the sensation platform 
 */

package de.cmlab.ubicom.sensor;

