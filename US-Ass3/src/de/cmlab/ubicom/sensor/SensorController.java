package de.cmlab.ubicom.sensor;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import de.cmlab.ubicom.common.SerialInterface;

/**
 * Helper class which communicates with the electronic sensor board over a
 * {@link SerialInterface}
 * 
 * @author pete
 * 
 */
public class SensorController {

	private SerialInterface sif;

	/**
	 * Main constructor, which uses the specified serial port for communication
	 * with the electronic sensor board
	 * 
	 * @param serialPort
	 *            String which specifies the serial port to use for
	 *            communication, something like /dev/ttyUSBX on linux/unix
	 *            systems - make sure you have appropriate rights to access this
	 *            device!
	 * @throws PortInUseException 
	 * @throws NoSuchPortException 
	 */
	public SensorController(String serialPort) throws NoSuchPortException, PortInUseException{
		this.sif = new SerialInterface(serialPort);
	}
	
	/**
	 * opens the connection to the esb
	 */
	public void openConnection(){
		sif.connect();		
		
		try {
			sif.sendCommand("sbp 1");
			Thread.sleep(200);
			sif.sendCommand("sbp");
			Thread.sleep(20);
			sif.sendCommand("slg 1");
		} catch (InterruptedException e) {
			// nothing to do here
		}
	}
	
	/**
	 * Reads the current temperature from the electronic sensor board
	 * 
	 * @return the current temperature measured by the ESB
	 */
	public String readTemperature() {
		String data = "";
		try {
			sif.sendCommand("rtt");
			Thread.sleep(100);
			data = sif.getLastData();
		} catch (InterruptedException e) {

		}

		return data;
	}

}
