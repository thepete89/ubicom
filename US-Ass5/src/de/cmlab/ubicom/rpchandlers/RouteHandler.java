package de.cmlab.ubicom.rpchandlers;

import java.io.IOException;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;
import com.thetransactioncompany.jsonrpc2.util.NamedParamsRetriever;

import de.cmlab.ubicom.util.ConsoleLogger;

/**
 * Implementation of the JSON-RPC2 RequestHandler interface, used to handle route requests from mobile/web app.
 * This sends back route data and calculates traffic jam probability for the user (at the moment, this is just a random number)
 * @author Pete
 *
 */
public class RouteHandler implements RequestHandler {

	@Override
	public String[] handledRequests() {
		return new String[] { "route" };
	}

	@Override
	public JSONRPC2Response process(JSONRPC2Request request,
			MessageContext requestCtx) {
		if (request.getMethod().equals("route")) {
			Map<String, Object> params = request.getNamedParams();
			NamedParamsRetriever np = new NamedParamsRetriever(params);
			try {
				String code = np.getString("code");
				
				String geojson = readFile("dummydata.geojson", Charset.defaultCharset());
				
				// some traffic jam magic here
				// create number formater with ROOT locale to preserve '.' as decimal seperator
				NumberFormat nf = NumberFormat.getNumberInstance(Locale.ROOT);
				DecimalFormat df = (DecimalFormat) nf;
				df.applyPattern("#.##");
				df.setRoundingMode(RoundingMode.HALF_UP);
				double jamProbability = Math.random();
				String jamIndicator = df.format(jamProbability);
				
				ConsoleLogger.getInstance().printDebug("Calculated a jam probability of " + jamIndicator, "RouteHandler");
				
				String response = "{ \"code\" : \"" + code
						+ "\", \"jamfactor\" : \"" + jamIndicator + "\", \"geodata\" : " + geojson + "}";

				return new JSONRPC2Response(response, request.getID());
			} catch (JSONRPC2Error e) {
				ConsoleLogger.getInstance().printError("Incorrect JSON query!",
						"RouteHandler");
				e.printStackTrace();
				return new JSONRPC2Response(JSONRPC2Error.INVALID_PARAMS,
						request.getID());
			} catch (IOException e) {
				ConsoleLogger.getInstance().printError("Could not read data file!", "RouteHandler");
				e.printStackTrace();
				return new JSONRPC2Response(JSONRPC2Error.INTERNAL_ERROR, request.getID());
			}

		} else {
			return new JSONRPC2Response(JSONRPC2Error.METHOD_NOT_FOUND,
					request.getID());
		}
	}

	// simple method to read contents of a datafile
	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}

}
