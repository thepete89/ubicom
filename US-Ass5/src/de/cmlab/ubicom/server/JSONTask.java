package de.cmlab.ubicom.server;

import java.io.IOException;
import java.io.PrintStream;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;

import de.cmlab.ubicom.util.ConsoleLogger;

/**
 * JSONTask handles a http request from a client and dispatches its json data to the JSONRPC2 dispatcher.
 * When the request is processed, a corresponding reply is send back to the client. 
 * @author Pete
 *
 */
public class JSONTask implements Runnable {

	private final Response response;
	private final Request request;
	private final Dispatcher jsonDispatcher;

	public JSONTask(Request request, Response response, Dispatcher dispatcher) {
		this.response = response;
		this.request = request;
		this.jsonDispatcher = dispatcher;
	}

	@Override
	public void run() {
		try {

			ConsoleLogger.getInstance().printInfo(
					"Handling request from: "
							+ request.getClientAddress().getHostString(),
					"HANDLER " + Thread.currentThread().getId());

			PrintStream body = response.getPrintStream();
			long time = System.currentTimeMillis();

			ConsoleLogger.getInstance().printDebug(
					"REQUEST-INFORMATION: \n" + request.toString(),
					"HANDLER " + Thread.currentThread().getId());
			ConsoleLogger.getInstance().printDebug(
					"REQUEST-DATA: \n" + request.getContent(),
					"HANDLER " + Thread.currentThread().getId());

			JSONRPC2Response jsonResponse = jsonDispatcher.process(
					JSONRPC2Request.parse(request.getContent()), null);

			ConsoleLogger.getInstance().printDebug(
					"JSON-RESPONSE: \n " + jsonResponse.toString(),
					"HANDLER " + Thread.currentThread().getId());

			response.setValue("Content-Type", "application/json");
			// IMPORTANT: this is hardcoded to allow parsing of json on
			// client-side, otherwise we would have an issue with same origin
			// policy
			response.setValue("Access-Control-Allow-Origin", "*");
			response.setValue("Server", "JSONRPC-Testserver (based on Simple)");
			response.setDate("Date", time);
			response.setDate("Last-Modified", time);
			body.print(jsonResponse.toString());
			body.close();
			ConsoleLogger.getInstance().printDebug(
					"RESPONSE-HEADER: \n" + response.toString(),
					"HANDLER " + Thread.currentThread().getId());
			response.close();

		} catch (IOException | JSONRPC2ParseException e) {
			ConsoleLogger.getInstance().printError("Could not Handle Request:",
					"HANDLER " + Thread.currentThread().getId());
			e.printStackTrace();
		}
	}

}
