package de.cmlab.ubicom.server;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.util.thread.PoolExecutor;

import com.thetransactioncompany.jsonrpc2.server.Dispatcher;

import de.cmlab.ubicom.rpchandlers.RouteHandler;
import de.cmlab.ubicom.util.ConsoleLogger;

/**
 * Implementation of the Container interface from the simpleframework http
 * framework, used by the http server process. This creates a threadpool that
 * handles requests to the server via instances of {@link JSONTask} runnables.
 * 
 * @author Pete
 * 
 */
public class JSONRequestHandler implements Container {

	private final PoolExecutor executor;
	private final Dispatcher jsonDispatcher;

	/**
	 * Creates a new request handler with a threadpool of given size.
	 * 
	 * @param size
	 *            the size the threadpool uses
	 */
	public JSONRequestHandler(int size) {
		this.jsonDispatcher = new Dispatcher();
		jsonDispatcher.register(new RouteHandler());
		this.executor = new PoolExecutor(JSONTask.class, size);
		ConsoleLogger.getInstance().printInfo(
				"RequestHandler-Executor started", "EXECUTOR");
	}

	@Override
	public void handle(Request request, Response response) {
		JSONTask task = new JSONTask(request, response, jsonDispatcher);
		executor.execute(task);
	}

	public void stop() {
		executor.stop();
	}

}
