package de.cmlab.ubicom.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.Server;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;

import de.cmlab.ubicom.util.ConsoleLogger;

/**
 * This is the main class for the JSON-RPC server which handles communication with the client
 * 
 * @author Pete
 *
 */
public class JSONServerMain {
	
	private static ConsoleLogger logger;
	
	public static void main(String[] args) throws IOException {
		logger = ConsoleLogger.getInstance();
		
		if(args.length < 1){
			logger.printInfo("USAGE: rpcserver.jar <port>", "MAIN");
			System.exit(0);
		}
		
		int port = Integer.parseInt(args[0]);
		logger.printInfo("JSON RPC Server starting on port" + port, "MAIN");
		
		JSONRequestHandler container = new JSONRequestHandler(10);
		Server server = new ContainerServer(container);
		Connection connection = new SocketConnection(server);
		SocketAddress address = new InetSocketAddress(port);
		
		logger.printInfo("Server listening on port " + port, "MAIN");
		logger.printInfo("Type q and press enter to stop", "MAIN");
		
		connection.connect(address);
		
		while(true){
			try {
				if(System.in.available() > 0){
					char key = (char) System.in.read();
					if("q".equalsIgnoreCase(String.valueOf(key))){
						// leave this loop
						break;
					}
				}
			} catch (IOException e) {
			}
		}
		
		logger.printWarn("Server is stopping now!", "MAIN");
		container.stop();
		server.stop();
		connection.close();

	}

}
