package de.cmlab.ubicom.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Simple console logger for convenient console output
 * 
 * @author Pete
 *
 */
public class ConsoleLogger {
	
	// date format for logger
	private static final String DATE_FORMAT = "yyyy.MM.dd HH:mm:ss";
	// date formatter
	private final SimpleDateFormat dateFormater;
	// logger object itself, initialized at first access through the virtual machine for thread-safety
	private static ConsoleLogger logger = new ConsoleLogger();
	
	private ConsoleLogger(){
		// private constructor, initializes the formatter with given date format
		dateFormater = new SimpleDateFormat(DATE_FORMAT);
	}
	
	/**
	 * Returns an instance of the console logger
	 * 
	 * @return
	 * 	ConsoleLogger instance
	 */
	public static ConsoleLogger getInstance(){
		
		return logger;
	}
	
	/**
	 * Print debug message
	 * @param data
	 * 	the message to print
	 * @param source
	 * 	the source of the message (e.g. threadname/id, mainclass identifier etc.)
	 */
	public void printDebug(String data, String source){
		System.out.println("[DEBUG][" + source + "] [" + dateFormater.format(new Date()) + "] " + data);
	}
	
	/**
	 * Print info message
	 * @param data
	 * @param source
	 */
	public void printInfo(String data, String source){
		System.out.println("[INFO][" + source + "] [" + dateFormater.format(new Date()) + "] " + data);
	}
	
	/**
	 * Print warning message
	 * @param data
	 * @param source
	 */
	public void printWarn(String data, String source){
		System.out.println("[WARNING][" + source + "] [" + dateFormater.format(new Date()) + "] " + data);
	}
	
	/**
	 * print error message on stderr
	 * @param data
	 * @param source
	 */
	public void printError(String data, String source){
		System.err.println("[ERROR][" + source + "] [" + dateFormater.format(new Date()) + "] " + data);
	}
}
