package de.cmlab.ubicomp.rpcclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xmlrpc.XmlRpcClient;

/**
 * main rpc client class and entry point of the client program
 * 
 * @author Pete
 * 
 */
public class ClientMain {

	public static void main(String[] args) {
		String server = "";
		if (args.length < 1) {
			System.out
					.println("Enter the address of the rpc server (Format: http://address-to-server.tld:port/xmlrpc)");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			try {
				server = reader.readLine();
			} catch (IOException e) {
				System.err.println("Could not read user input - exiting!");
				System.exit(-1);
			}
		} else {
			server = args[0];
		}

		System.out.println("Connecting to " + server);

		try {
			URL serverUrl = new URL(server);
			XmlRpcClient rpcClient = new XmlRpcClient(serverUrl);
			ClientUserInterface userInterface = new ClientUserInterface(
					rpcClient);
			userInterface.runUi();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		System.out.println("Leaving programm");
	}

}
