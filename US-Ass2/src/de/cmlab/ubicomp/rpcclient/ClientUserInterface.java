package de.cmlab.ubicomp.rpcclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

/**
 * ClientUserInterface - handles user input and communication with the rpc
 * server
 * 
 * @author Pete
 * 
 */
public class ClientUserInterface {

	private BufferedReader consoleReader;
	private XmlRpcClient rpcClient;

	/**
	 * User interface constructor - expects an active and connected instance of an rpc client
	 * as input parameter
	 * 
	 * @param rpcClient
	 *            the rpc client used for this connection
	 */
	public ClientUserInterface(XmlRpcClient rpcClient) {
		super();
		this.rpcClient = rpcClient;
		consoleReader = new BufferedReader(new InputStreamReader(System.in));
	}

	/**
	 * runs the ui
	 */
	public void runUi() {
		showHelp();
		while (true) {
			try {
				String input = consoleReader.readLine();

				if ("exit".equals(input)) {
					break;
				} else {
					switch (input) {
					case "toggleGreen":
						toggleGreen();
						break;
						
					case "toggleRed":
						toggleRed();
						break;
						
					case "toggleYellow":
						toggleYellow();
						break;
						
					case "readTemp":
						readTemp();
						break;
					
					case "readBat":
						readBat();
						break;
						
					case "help":
						showHelp();
						break;
						
					default:
						System.out.println("ERROR: Unknown command");
						showHelp();
						break;
					}
				}

			} catch (IOException e) {
			}
		}
	}
	
	private void showHelp(){
		System.out.println(" --- Available Commands --- ");
		System.out.println(" * toggleGreen - toggles green led of the esb");
		System.out.println(" * toggleRed - toggles red led of the esb");
		System.out.println(" * toggleYellow - toggles yellow led of the esb");
		System.out.println(" * readTemp - reads temperature sensor of the esb");
		System.out.println(" * readBat - reads battery sensor of the esb");
		System.out.println(" * help - show this help");
		System.out.println(" * exit - quit this program");
		System.out.println(" --------------------------- ");
	}
	
	private void toggleGreen(){
		try {
			Vector<Object> params = new Vector<>();
			params.addElement("toggleGreen");
			String result = (String) rpcClient.execute("command.toggleGreen", params);
			System.out.println("Server respondet:" + result);
		} catch (XmlRpcException e) {
			System.out.println("Could not send command: ");
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println("IOException while sending command:");
			e.printStackTrace();
		}
	}
	
	private void toggleRed(){
		try {
			Vector<Object> params = new Vector<>();
			params.addElement("toggleRed");
			String result = (String) rpcClient.execute("command.toggleRed", params);
			System.out.println("Server respondet:" + result);
		} catch (XmlRpcException e) {
			System.out.println("Could not send command: ");
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println("IOException while sending command:");
			e.printStackTrace();
		}
	}
	
	private void toggleYellow(){
		try {
			Vector<Object> params = new Vector<>();
			params.addElement("toggleYellow");
			String result = (String) rpcClient.execute("command.toggleYellow", params);
			System.out.println("Server respondet:" + result);
		} catch (XmlRpcException e) {
			System.out.println("Could not send command: ");
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println("IOException while sending command:");
			e.printStackTrace();
		}
	}
	
	private void readTemp(){
		try {
			Vector<Object> params = new Vector<>();
			params.addElement("readTemp");
			String result = (String) rpcClient.execute("command.readTemp", params);
			System.out.println("Server respondet:" + result);
		} catch (XmlRpcException e) {
			System.out.println("Could not send command: ");
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println("IOException while sending command:");
			e.printStackTrace();
		}
	}
	
	private void readBat(){
		try {
			Vector<Object> params = new Vector<>();
			params.addElement("readBat");
			String result = (String) rpcClient.execute("command.readBat", params);
			System.out.println("Server respondet:" + result);
		} catch (XmlRpcException e) {
			System.out.println("Could not send command: ");
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println("IOException while sending command:");
			e.printStackTrace();
		}
	}

}
