package de.cmlab.ubicomp.rpcserver;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.xmlrpc.WebServer;

import de.cmlab.ubicomp.common.SerialInterface;

/**
 * main rpc server class and entry point of the server program
 * 
 * @author Pete
 * 
 */
public class ServerMain {

	static SerialInterface sif;

	public static void main(String[] args) {
		// setting up config
		String configFile = "";
		if (args.length < 1) {
			System.out
					.println("No configuration file specified at startup - trying with default (ServerConfig.txt)");
			configFile = "ServerConfig.txt";
		} else {
			configFile = args[0];
		}

		System.out.println("Loading configuration");
		Properties config = new Properties();

		InputStream cis;
		try {
			cis = new FileInputStream(configFile);
			config.load(cis);
		} catch (FileNotFoundException e1) {
			System.out
					.println("WARNING: Configuration file not found - using defaults!");
		} catch (IOException e) {
			System.err
					.println("ERROR: IOError while loading configuration - leaving program!");
			System.exit(-1);
		}

		System.out
				.println("Configuration loaded - creating and initializing serial link");

		try {
			String comPort = config.getProperty("SERIAL", "/dev/ttyUSB0");
			sif = new SerialInterface(comPort);
		} catch (NoSuchPortException | PortInUseException e) {
			System.err
					.println("ERROR: Could not create serial link - leaving program!");
			System.exit(-1);
		}

		sif.connect();

		// after connection: do a short beep to signalize
		// successful connection
		try {
			sif.sendCommand("sbp 1");
			Thread.sleep(200);
			sif.sendCommand("sbp");
		} catch (InterruptedException e) {
			// nothing to do here
		}

		System.out.println("Serial link to esb established");

		// start xml-rpc server
		int port = new Integer(config.getProperty("SERVERPORT", "8080"));
		System.out.println("Starting XML-RPC-Server on port " + port);
		WebServer server = new WebServer(port);
		server.addHandler("command", new CommandController(sif));
		server.setParanoid(false);
		server.start();
	}

}
