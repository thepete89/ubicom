package de.cmlab.ubicomp.rpcserver;

import de.cmlab.ubicomp.common.SerialInterface;

/**
 * CommandController - used from the xml-rpc server to control the esb sensor
 * board via the {@link SerialInterface}
 * 
 * @author Pete
 * 
 */
public class CommandController {

	private SerialInterface sif;

	/**
	 * Constructor
	 * 
	 * @param serialInterface
	 *            an instance of the {@link SerialInterface} used for
	 *            communication
	 */
	public CommandController(SerialInterface serialInterface) {
		super();
		this.sif = serialInterface;
	}

	/**
	 * toggles the green led of the esb
	 * 
	 * @param data
	 *            dummy data send by the client
	 * @return simple string, in this case just OK as the esb does not return
	 *         anything
	 */
	public String toggleGreen(String data) {
		System.out.println("Received toggleGreen command - data: " + data);
		sif.sendCommand("swg");
		return "OK";
	}

	/**
	 * toggles the red led of the esb
	 * 
	 * @param data
	 *            dummy data send by the client
	 * @return simple string, in this case just OK as the esb does not return
	 *         anything
	 */
	public String toggleRed(String data) {
		System.out.println("Received toggleRed command - data: " + data);
		sif.sendCommand("swr");
		return "OK";
	}

	/**
	 * toggles the yellow led of the esb
	 * 
	 * @param data
	 *            dummy data send by the client
	 * @return simple string, in this case just OK as the esb does not return
	 *         anything
	 */
	public String toggleYellow(String data) {
		System.out.println("Received toggleYellow command - data: " + data);
		sif.sendCommand("swy");
		return "OK";
	}

	/**
	 * reads the temperature sensor of the esb
	 * 
	 * @param data
	 *            dummy data send by the client
	 * @return string containing OK and the read temperature which is send back
	 *         to the client
	 */
	public String readTemp(String data) {
		System.out.println("Received readTemp command - data: " + data);
		sif.sendCommand("rtt");
		// needs a little waittime here or we get errors
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// nothing to do here;
		}
		String esbData = sif.getLastData();
		System.out.println("TEMP-Reading: " + esbData);
		return "OK " + esbData;
	}

	/**
	 * reads the battery sensor of the esb
	 * 
	 * @param data
	 *            dummy data send by the client
	 * @return string containing OK and the read value of the battery sensor
	 */
	public String readBat(String data) {
		System.out.println("Received readBat command - data: " + data);
		sif.sendCommand("rvb");
		// needs a little waittime here or we get errors
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// nothing to do here;
		}
		String esbData = sif.getLastData();
		// fix for corrupted string with invalid char at end
		String formatted = esbData.substring(0, esbData.length() - 1);

		System.out.println("BAT-Reading: " + formatted);
		return "OK " + formatted;
	}
}
